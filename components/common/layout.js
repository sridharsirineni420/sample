import "babel-polyfill";
import "isomorphic-unfetch";
import Head from "next/head";
import Business from "./business";
import "../../app/global.scss";
import { Link } from "../../app/routes";

class Layout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div>
        <Head>
          <title>
            {this.props.title
              ? this.props.title
              : "Door | Real Estate | Listings, Home Values, &amp; No Commissions |"}
          </title>
          <meta
            name="description"
            content={
              this.props.description
                ? this.props.description
                : 'Door is a Texas-based residential real estate brokerage. Buy a home or sell a home with Door - just $5,000 flat fee to list your home."'
            }
          />
        </Head>
        <div>
          <ul>
            <li>
              <Link to="/">
                <a>Home</a>
              </Link>
            </li>
            <li>
              <Link to="/about">
                <a>About</a>
              </Link>
            </li>
          </ul>
        </div>
        {this.props.children}
      </div>
    );
  }
}
export default Business(Layout);
