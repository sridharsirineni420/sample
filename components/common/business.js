import Router from "next/router";

export default function Business(Child) {
  return class WrappedComponent extends React.Component {
    static getInitialProps(context) {
      return Child.getInitialProps(context);
    }

    componentDidMount() {
      Router.onRouteChangeComplete = url => {
        // pass router change event to Google Analytics w/ page title
      };

    }

    componentDidCatch(error, info) {
    }

    render() {
      return <Child {...this.props} />;
    }
  };
}