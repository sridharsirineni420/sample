# Purpose
This repo contains the frontend for the Door Homes website(door.com) built with Next.js, React, and Node.js providing server-side rendering.

Please provide the appropriate ".env" enviornmental variables file in the root of the directory before proceeding.

# Launch in Dev Mode with HMR:

```sh
$ docker-compose build --force-rm --no-cache
$ docker-compose up
```

Navigate your browser to ``http://localhost:9010``. You should see changes to components happen automatically with HMR.

If you have problems, try force recreating the container.
``` docker-compose up --force-recreate```

# Launch in Production Mode: 
```sh
$ docker build -f ./dockerfile-prod -t door-nextjs .
$ docker run -p 9010:9010 door-nextjs
```

PM2 in Cluster Mode is being used within the container for persistance of both Next and the Node API. Note: ```next build``` is automatically called to build the files for production.