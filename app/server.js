const next = require('next');
const routes = require('./routes');
const path = require('path');

const app = next({
  dev: process.env.NODE_ENV !== 'production'
});
const handler = routes.getRequestHandler(app);
const port = 9010;
const express = require('express');

app.prepare().then(() => {
  express().use(handler).listen(port, '0.0.0.0');
  console.log(`Next.JS is listening on port ${port}`);
});