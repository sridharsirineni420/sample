import React from "react";
import Layout from "../components/common/layout";
import fetch from "isomorphic-unfetch";
import Banner from "../components/homepage/banner";

class Index extends React.Component {
  static async getInitialProps() {
    // eslint-disable-next-line no-undef
    const res = await fetch("https://api.github.com/repos/zeit/next.js");
    const json = await res.json();
    return { stars: json.stargazers_count };
  }

  componentDidMount() {
    console.log(this.props.stars);
  }

  render() {
    return (
      <Layout title="Door Homes" description="Test">
        <h1>This is a test</h1>
        <p>{process.env.TEST}</p>
        <Banner example="banner" />
      </Layout>
    );
  }
}

export default Index;
