import React from "react";
import Layout from "../components/common/layout";
import fetch from "isomorphic-unfetch";

class About extends React.Component {
  static async getInitialProps() {
    // eslint-disable-next-line no-undef
    const res = await fetch("https://api.github.com/repos/zeit/next.js");
    const json = await res.json();
    return { stars: json.stargazers_count };
  }

  componentDidMount() {
    console.log(this.props.stars);
  }

  render() {
    return (
      <Layout title="Door Homes 2" description="Test 2">
        <h1>Hey from About Page 2</h1>
        <img src="/static/door-icon.jpg" />
      </Layout>
    );
  }
}

export default About;